#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
namespace Novasloth {

    [System.Serializable]
    public class Checklist {

        public string Title { get { return this.title; } }
        public bool Toggle {
            get { return this.toggle; }
            set { this.toggle = value; }
        }
        public List<ChecklistItem> ItemList { get { return this.itemList; } }
        public string NewItemTextField {
            get { return this.newItemTextField; }
            set { this.newItemTextField = value; }
        }

        [SerializeField] private string title;
        [SerializeField] private bool toggle;
        [SerializeField] private List<ChecklistItem> itemList;
        [SerializeField] private string newItemTextField;

        public Checklist (string title) {
            this.title = title;
            this.toggle = true;
            this.itemList = new List<ChecklistItem>();
            this.newItemTextField = "";
        }
    }
}
#endif