#if UNITY_EDITOR
using UnityEngine;

// Lee Barton
namespace Novasloth {

    [System.Serializable]
    public class ChecklistItem {

        public string Text {
            get { return this.text; }
            set { this.text = value; }
        }
        public bool Done {
            get { return this.done; }
            set { this.done = value; }
        }

        [SerializeField] private string text;
        [SerializeField] private bool done;

        public ChecklistItem (string text) {
            this.text = text;
            this.done = false;
        }
    }
}
#endif